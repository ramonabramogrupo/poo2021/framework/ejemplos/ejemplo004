<?php

/* @var $this yii\web\View */

$this->title = 'My Yii Application';
?>
<div class="site-index">

    <div class="jumbotron">
        <h1>Ejemplo numero 4</h1>

        <p class="lead">Introduciendonos al mundo del framework Yii</p>

    </div>

    <div class="body-content">

        <div class="row">
            <div class="col-lg-4">
                <h2>Primer objetivo</h2>

                <p>Vamos a explicar el funcionamiento basico de una aplicacion</p>

            </div>
            <div class="col-lg-4">
                <h2>Segundo objetivo</h2>

                <p>Configuracion de la aplicacion</p>

            </div>
            <div class="col-lg-4">
                <h2>Tercer Objetivo</h2>

                <p>Crear una aplicacion (CRUD) para realizar altas, bajas y modificaciones</p>

            </div>
        </div>

    </div>
</div>
